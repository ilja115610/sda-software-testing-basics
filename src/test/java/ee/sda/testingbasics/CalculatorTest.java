package ee.sda.testingbasics;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    // Annotations
    // Test method
    @Test
    public void addAndMultiplyByThreeTest(){

        Calculator calculator = new Calculator();

        //I changed something

        int actualResult = calculator.AddAndMultiplyByThree(2,2);

        Assertions.assertEquals(12, actualResult);
    }


}
